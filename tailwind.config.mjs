import { fontFamily } from 'tailwindcss/defaultTheme'

/** @type {import('tailwindcss').Config} */
export default {
    content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
    theme: {
        extend: {
            colors: ({ colors }) => {
                return { primary: colors.slate, secondary: colors.blue }
            },
            fontFamily: {
                sans: ['"Rubik Variable"', ...fontFamily.sans],
            },
        },
    },
    plugins: [],
}
