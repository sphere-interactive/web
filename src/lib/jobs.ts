import { sql } from '@/lib/db'

export async function getJobs(limit: number = 5) {
    try {
        const result = await sql<
            {
                id: string
                name: string
                description: string
                department: string
                full_time: boolean
                category: number
            }[]
        >`SELECT * FROM sphere_jobs LIMIT ${limit}`

        const levels = new Map<number, string>([
            [0, 'Student/Entry level'],
            [1, 'Professionals'],
        ])

        const mappedResult = result.map((r) => ({
            id: r.id,
            name: r.name,
            description: r.description,
            department: r.department,
            period: r.full_time ? 'Full-time' : 'Part-time',
            category: levels.get(r.category) ?? 'Unknown',
        }))

        return mappedResult
    } catch (err: any) {
        return []
    }
}
