export const DISCORD = import.meta.env.PUBLIC_DISCORD as string
export const WHATSAPP = import.meta.env.PUBLIC_WHATSAPP as string
export const DB_URL = import.meta.env.DB_URL as string
