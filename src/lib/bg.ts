import { getImage } from 'astro:assets'

import One from '@/images/1.png'
import Two from '@/images/2.png'
import Three from '@/images/3.png'
import Four from '@/images/4.png'
import Five from '@/images/5.png'
import Six from '@/images/6.png'
import Seven from '@/images/7.png'
import Eight from '@/images/8.png'
import Nine from '@/images/9.png'
import Ten from '@/images/10.png'
import Eleven from '@/images/11.png'
import Twelve from '@/images/12.png'

const FORMAT = 'avif'

const allImages = [One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Eleven, Twelve]
const allMappedImages = await Promise.all(
    allImages.map(async (i) => await getImage({ src: i, format: FORMAT })),
)

export function getRandomImage() {
    const randomId = Math.floor(Math.random() * allMappedImages.length)
    return allMappedImages[randomId]
}
