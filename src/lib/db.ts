import postgres from 'postgres'

import { DB_URL } from '@/lib/env'

export const sql = postgres(DB_URL)
