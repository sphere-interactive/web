import { defineConfig } from 'astro/config'

import icon from 'astro-icon'
import tailwind from '@astrojs/tailwind'
import vercel from '@astrojs/vercel/serverless'

// https://astro.build/config
export default defineConfig({
    // Integrations
    integrations: [tailwind(), icon()],

    // Output
    output: 'hybrid',
    adapter: vercel({
        imageService: true,
        devImageService: 'sharp',
    }),
})
